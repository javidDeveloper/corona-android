<h3><b>This project was developed with Kotlin and uses the following Technologies</b></h3><br>
<hr>
<b>architecture:</b> MVI, Clean Architecture<br>
<b>Jetpack:</b> Navigation Component, DataBinding, ConstraintLayout<br>
<b>Connect to Server:</b> Coroutine, Retrofit<br>
<b>Dependency Injection for class and ViewModels:</b> Dagger Hilt<br>
<b>Image Loader:</b> Coil<br>
<hr>
