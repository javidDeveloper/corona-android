package ir.javid.sattar.coronastatistics.view.countryList

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.javid.sattar.core_data.coronaData.entity.CovidEntity
import ir.javid.sattar.core_data.coronaData.service.CoronaService
import ir.javid.sattar.core_data.ServiceResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * @author  : j.sattar
 * @since   : 6/7/2021 -- 3:18 PM
 * @summary : Corona Statistics
 */

@HiltViewModel
class CoronaListVM @Inject constructor(
    private val service: CoronaService
) : ViewModel() {

    var loadingState: Boolean = false
    var covidListLD: MutableLiveData<List<CovidEntity>> = MutableLiveData()
    var errorLD: MutableLiveData<Any> = MutableLiveData()

    fun requestAllCoronaList() = viewModelScope.launch(Dispatchers.IO) {
        service.requestAllCountry {
            when (it) {
                is ServiceResult.Error -> {
                }
                is ServiceResult.Loading -> loadingState = true
                is ServiceResult.RestError -> {
                    loadingState = false
                    errorLD.postValue(it.error)
                }
                is ServiceResult.Success -> {
                    loadingState = false
                    covidListLD.postValue(it.data)
                }

            }
        }
    }
}
