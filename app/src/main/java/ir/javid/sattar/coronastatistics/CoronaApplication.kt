package ir.javid.sattar.coronastatistics

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import ir.javid.sattar.core_data.tools.AppCore
import javax.inject.Inject


/**
 * @author  : j.sattar
 * @since   : 6/7/2021 -- 2:27 PM
 * @summary : Corona Statistics
 */
@HiltAndroidApp
class CoronaApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        AppCore.initCore(applicationContext)
    }

}