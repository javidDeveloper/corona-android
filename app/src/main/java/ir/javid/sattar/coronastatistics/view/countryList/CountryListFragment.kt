package ir.javid.sattar.coronastatistics.view.countryList

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ir.javid.sattar.coronastatistics.databinding.FragmentCountryListBinding

@AndroidEntryPoint
class CountryListFragment : Fragment() {

    private val vm: CoronaListVM by viewModels()
    private lateinit var mBinding: FragmentCountryListBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = FragmentCountryListBinding.inflate(inflater, container, false)
        vm.requestAllCoronaList()
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding.apply {
//            vm.covidListLD.observe(viewLifecycleOwner) {
//
//            }

        }
    }

}