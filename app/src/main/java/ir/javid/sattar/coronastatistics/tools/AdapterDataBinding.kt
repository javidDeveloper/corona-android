package ir.javid.sattar.coronastatistics.tools

import android.view.View
import androidx.databinding.BindingAdapter


/**
 * @author  : j.sattar
 * @since   : 6/9/2021 -- 10:13 AM
 * @summary : Corona Statistics
 */

@BindingAdapter(value = ["bind:visibleOrGone"])
fun View.visibleOrGone(isVisible: Boolean) = when (isVisible) {
    true -> visibility = View.VISIBLE
    false -> visibility = View.GONE
}