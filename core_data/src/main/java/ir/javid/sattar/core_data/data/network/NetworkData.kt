package ir.javid.sattar.core_data.data.network


/**
 * @author  : j.sattar
 * @since   : 6/8/2021 -- 9:27 AM
 * @summary : Corona Statistics
 */

internal const val BASE_URL = "https://covid-19.dataflowkit.com/"