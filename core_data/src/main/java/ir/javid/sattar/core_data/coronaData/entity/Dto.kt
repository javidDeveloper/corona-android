package ir.javid.sattar.core_data.coronaData.entity


/**
 * @author  : javid
 * @since   : 2021/Feb -- 10:03 PM
 * @summary : --
 */
internal interface Dto {
    fun toEntity(): Entity
}