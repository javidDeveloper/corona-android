package ir.javid.sattar.core_data.data

import ir.javid.sattar.core_data.ServiceResult


/**
 * @author  : j.sattar
 * @since   : 6/9/2021 -- 8:45 AM
 * @summary : Corona Statistics
 */

internal class ServiceResultDataWrapper<in R, in SE, out SR>(
    private val result: ((ServiceResult<SR>) -> Unit)? = null,
    private val onSuccess: ((R) -> Unit)? = null,
    private val onRestErrorError: ((SE) -> Unit)? = null,
    private val onError: ((Throwable) -> Unit)? = null,
)/* : Response<R>*/ {
}