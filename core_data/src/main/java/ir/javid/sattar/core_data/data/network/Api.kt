package ir.javid.sattar.core_data.data.network

import ir.javid.sattar.core_data.coronaData.entity.CovidDto
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path


/**
 * @author  : j.sattar
 * @since   : 6/8/2021 -- 9:29 AM
 * @summary : Corona Statistics
 */

internal interface Api {
    @GET("v1")
    suspend fun requestAllCovidList() : Response<List<CovidDto>>


    @GET("v1/{country}")
    suspend fun requestCovidByCountry(@Path("country") country: String): Response<CovidDto>
}