package ir.javid.sattar.core_data.coronaData.entity

import com.google.gson.annotations.SerializedName

/**
 * @author  : javid
 * @since   : 2020/Sep -- 3:42 PM
 * @summary : --
 */
internal data class CovidDto(
    @SerializedName("Active Cases_text")    val activeCasesText: String,
    @SerializedName("Country_text")         val countryText: String,
    @SerializedName("Last Update")          val lastUpdate: String,
    @SerializedName("New Cases_text")       val newCasesText: String,
    @SerializedName("New Deaths_text")      val newDeathsText: Int,
    @SerializedName("Total Cases_text")     val totalCasesText: String,
    @SerializedName("Total Deaths_text")    val totalDeathsText: String,
    @SerializedName("Total Recovered_text") val totalRecoveredText: String
) : Dto {
    override fun toEntity() = CovidEntity(
        activeCasesText,
        countryText,
        lastUpdate,
        newCasesText,
        newDeathsText,
        totalCasesText,
        totalDeathsText,
        totalRecoveredText,
    )
}