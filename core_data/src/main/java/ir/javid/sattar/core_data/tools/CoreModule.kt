package ir.javid.sattar.core_data.tools

import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ir.javid.sattar.core_data.data.network.Api
import ir.javid.sattar.core_data.data.network.BASE_URL
import ir.javid.sattar.core_data.data.network.interceptors.RequestInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
internal object CoreModule {

    @Singleton
    @Provides
    fun apiProvider(): Api =
        Retrofit.Builder()
            .baseUrl("https://covid-19.dataflowkit.com/")
//            .client(
//                OkHttpClient()
//                    .newBuilder()
//                    .connectTimeout(30, TimeUnit.SECONDS)
//                    .readTimeout(10, TimeUnit.SECONDS)
//                    .addInterceptor(RequestInterceptor())
//                    .build()
//            )
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build()
            .create(Api::class.java)


}


