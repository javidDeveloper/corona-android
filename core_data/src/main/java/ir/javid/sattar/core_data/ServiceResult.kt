package ir.javid.sattar.core_data


/**
 * @author  : j.sattar
 * @since   : 6/8/2021 -- 4:23 PM
 * @summary : Corona Statistics
 */

sealed class ServiceResult<T> {

    /**
     * loading state
     */
    class Loading<T> : ServiceResult<T>()

    /**
     * success state
     */
    data class Success<T>(val data: T?) : ServiceResult<T>()

    /**
     * rest error state
     */
    data class RestError<T>(val error: Any?) : ServiceResult<T>()

    /**
     * error state
     */
    data class Error<T>(val throwable: Throwable) : ServiceResult<T>()
}