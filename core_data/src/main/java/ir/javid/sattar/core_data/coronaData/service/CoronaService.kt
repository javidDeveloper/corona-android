package ir.javid.sattar.core_data.coronaData.service

import ir.javid.sattar.core_data.coronaData.entity.CovidEntity
import ir.javid.sattar.core_data.ServiceResult


/**
 * @author  : j.sattar
 * @since   : 6/8/2021 -- 4:09 PM
 * @summary : Corona Statistics
 */

interface CoronaService {

    /**
     * this function for get all country list with all statistics
     * @return listOf [CovidEntity]
     */
    suspend fun requestAllCountry(result: (ServiceResult<List<CovidEntity>>) -> Unit)

    /**
     * this function for get a country list with all statistics by country name
     * @return  [CovidEntity]
     */
    suspend fun requestCountry(countryName: String, result: (ServiceResult<CovidEntity>) -> Unit)
}