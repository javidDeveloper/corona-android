package ir.javid.sattar.core_data.coronaData.repository

import ir.javid.sattar.core_data.coronaData.entity.CovidDto
import retrofit2.Response


/**
 * @author  : j.sattar
 * @since   : 6/8/2021 -- 3:32 PM
 * @summary : Corona Statistics
 */

internal interface CoronaRepository {

    /**
     * this function for get all country list with all statistics
     * @return listOf [CovidDto]
     */
    suspend fun requestAllCountry(): Response<List<CovidDto>>

    /**
     * this function for get a country list with all statistics by country name
     * @return  [CovidDto]
     */
    suspend fun requestCountry(countryName: String): Response<CovidDto>
}