package ir.javid.sattar.core_data.coronaData.repository

import ir.javid.sattar.core_data.coronaData.entity.CovidDto
import ir.javid.sattar.core_data.data.network.Api
import retrofit2.Response
import javax.inject.Inject


/**
 * @author  : j.sattar
 * @since   : 6/8/2021 -- 4:02 PM
 * @summary : Corona Statistics
 */

internal class CoronaRepositoryImpl @Inject constructor(
    private val api: Api
) : CoronaRepository {
    override suspend fun requestAllCountry(): Response<List<CovidDto>> =
        api.requestAllCovidList()


    override suspend fun requestCountry(
        countryName: String
    ): Response<CovidDto> =
        api.requestCovidByCountry(countryName)
}