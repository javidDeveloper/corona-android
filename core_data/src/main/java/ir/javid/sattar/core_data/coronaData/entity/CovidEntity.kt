package ir.javid.sattar.core_data.coronaData.entity


/**
 * @author  : javid
 * @since   : 2020/Sep -- 3:42 PM
 * @summary : --
 */
data class CovidEntity(
    var activeCasesText: String,
    var countryText: String,
    var lastUpdate: String,
    var newCasesText: String,
    var newDeathsText: Int,
    var totalCasesText: String,
    var totalDeathsText: String,
    var totalRecoveredText: String
) : Entity