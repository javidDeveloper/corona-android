package ir.javid.sattar.core_data.data.network.interceptors

import okhttp3.Interceptor
import okhttp3.Response


/**
 * @author  : j.sattar
 * @since   : 6/8/2021 -- 9:51 AM
 * @summary : Corona Statistics
 */

internal class RequestInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response =
         chain.proceed(
            chain.request().apply {
                newBuilder().method(method,body)
            }
        )
    }
