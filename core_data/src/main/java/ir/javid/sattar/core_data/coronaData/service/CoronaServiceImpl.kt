package ir.javid.sattar.core_data.coronaData.service

import ir.javid.sattar.core_data.coronaData.entity.CovidEntity
import ir.javid.sattar.core_data.coronaData.repository.CoronaRepository
import ir.javid.sattar.core_data.ServiceResult
import javax.inject.Inject


/**
 * @author  : j.sattar
 * @since   : 6/8/2021 -- 4:15 PM
 * @summary : Corona Statistics
 */

internal class CoronaServiceImpl @Inject constructor(
    private val coronaRepository: CoronaRepository
) : CoronaService {
    override suspend fun requestAllCountry(
        result: (ServiceResult<List<CovidEntity>>) -> Unit
    ) {
        result(ServiceResult.Loading())
        coronaRepository.requestAllCountry().also { response ->
            if (response.isSuccessful) {
                response.body()
                    ?.map { dto ->
                        dto.toEntity()
                    }
                    ?.toList()
                    .also {
                        result(ServiceResult.Success(it))
                    }
            } else {
                result(ServiceResult.RestError(response.errorBody()))
            }
        }
    }

    override suspend fun requestCountry(
        countryName: String,
        result: (ServiceResult<CovidEntity>) -> Unit
    ) {

        coronaRepository.requestCountry(countryName).also { response ->
            if (response.isSuccessful) {
                response.body()?.also {
                    result(ServiceResult.Success(it.toEntity()))
                }
            } else {
                result(ServiceResult.RestError(response.errorBody()))
            }
        }

    }

}