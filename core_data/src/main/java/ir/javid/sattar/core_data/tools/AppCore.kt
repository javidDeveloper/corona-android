package ir.javid.sattar.core_data.tools

import android.content.Context


/**
 * @author  : j.sattar
 * @since   : 6/8/2021 -- 9:24 AM
 * @summary : Corona Statistics
 */


object AppCore {
    internal lateinit var context: Context

    fun initCore(context: Context) {
        this.context = context
    }
}