package ir.javid.sattar.core_data.tools

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ir.javid.sattar.core_data.coronaData.repository.CoronaRepository
import ir.javid.sattar.core_data.coronaData.repository.CoronaRepositoryImpl
import javax.inject.Singleton


/**
 * @author  : j.sattar
 * @since   : 6/9/2021 -- 11:49 AM
 * @summary : Corona Statistics
 */

@Module
@InstallIn(SingletonComponent::class)
internal object RepositoryModule {
    @Singleton
    @Binds
    fun bindCoronaRepository(
        coronaRepositoryImpl: CoronaRepositoryImpl
    ): CoronaRepository = coronaRepositoryImpl
}